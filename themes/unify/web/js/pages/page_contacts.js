var ContactPage = function () {

    return {
        
    	//Basic Map
        initMap: function () {
			var map;
			$(document).ready(function(){
			  map = new GMaps({
				div: '#map',
				scrollwheel: false,				
				lat: 59.929716,
				lng: 30.374614
			  });
			  
			  var marker = map.addMarker({
				lat: 59.929716,
				lng: 30.374614,
	            title: 'Finance Protected'
		       });
			});
        },

        //Panorama Map
        initPanorama: function () {
		    var panorama;
		    $(document).ready(function(){
		      panorama = GMaps.createPanorama({
		        el: '#panorama',
		        lat : 59.929716,
		        lng : 30.374614
		      });
		    });
		}        

    };
}();