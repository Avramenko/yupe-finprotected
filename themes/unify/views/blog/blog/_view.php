<!--Blog Post-->
<div class="blog margin-bottom-40">

    <div class="headline headline-md">
        <h2>
            <?php echo CHtml::link(
            CHtml::encode($data->name),
            ['/blog/blog/show/', 'slug' => CHtml::encode($data->slug)]
        ); ?>
        </h2>
    </div>
    <ul class="list-unstyled list-inline blog-info">
        <li><i class="fa fa-calendar"></i> <?php echo Yii::app()->getDateFormatter()->formatDateTime(
                $data->create_time,
                "long",
                false
            ); ?>
        </li>
        <li><i class="glyphicon glyphicon-user"></i> <?php echo CHtml::link(
                CHtml::encode($data->createUser->nick_name),
                ['/user/people/userInfo', 'username' => CHtml::encode($data->createUser->nick_name)]
            ); ?>
        </li>
        <li><i class="glyphicon glyphicon-pencil"></i> <?php echo CHtml::link(
                CHtml::encode($data->postsCount),
                ['/blog/post/blog/', 'slug' => CHtml::encode($data->slug)]
            ); ?>
        </li>
        <li>
            Проект / дело: <?php echo CHtml::link(
                $data->category->name,
                ['/blog/', 'category'=>CHtml::encode($data->category->slug)]
            ); ?>

        </li>
    </ul>

    <div class="blog-img">
        <?php echo CHtml::image(
            $data->getImageUrl(),
            CHtml::encode($data->name),
            [
                //'width' => 64,
                //'height' => 64,
                'class' => 'img-responsive'
            ]
        ); ?>

    </div>
    <?php echo $data->short; ?>
    <p>
        <?php echo CHtml::link(
            '<i class="fa fa-plus-sign"></i> Читать дальше ->',
            [
                '/blog/blog/show/',
                'slug' => CHtml::encode($data->slug)
            ],
            [
                'class' => 'btn-u btn-u-small'
            ]
        ); ?>
        <?php $this->widget(
            'application.modules.blog.widgets.JoinBlogWidget',
            ['user' => Yii::app()->user, 'blog' => $data]
        ); ?>
        <?php
        if ($data->userIn(Yii::app()->user->getId())) {
            echo CHtml::link(Yii::t('BlogModule.blog', 'Add a post'), ['/blog/publisher/write', 'blog-id' => $data->id], ['class' => 'btn btn-success btn-sm']);
        }
        ?>

    </p>
</div>
<!--End Blog Post-->
