<!--=== Blog Posts ===-->
<div class="">
    <div class="container content-sm">
        <div class="row">
            <!-- Blog All Posts -->
            <div class="col-md-9">
                <div class="headline headline-md"><h1>Проекты и дела</h1></div>
                <?php
                $this->widget(
                    'zii.widgets.CListView', array(
                        'dataProvider'=>$dataProvider,
                        'itemView'=>'_category',   // refers to the partial view named '_post'
                        /*'sortableAttributes'=>array(
                            'name'
                        ),*/
                    )
                );
                ?>
                </div>
            <div class="col-md-3">
                <?php if($this->beginCache('application.modules.blog.widgets.LastPostsWidget', ['duration' => $this->yupe->coreCacheTime])):?>
                    <?php $this->widget(
                        'application.modules.blog.widgets.LastPostsWidget',

                        [
                            'view' => 'lastposts-unify',
                            'cacheTime' => $this->yupe->coreCacheTime,

                        ]
                    ); ?>
                    <?php $this->endCache();?>
                <?php endif;?>

                <?php if($this->beginCache('application.modules.blog.widgets.BlogsCategoriesMenuWidget', ['duration' => $this->yupe->coreCacheTime])):?>
                    <?php $this->widget(
                        'application.modules.blog.widgets.BlogsCategoriesMenuWidget',

                        [
                            'view' => 'index',
                            'cacheTime' => $this->yupe->coreCacheTime,

                        ]
                    ); ?>
                    <?php $this->endCache();?>
                <?php endif;?>
                <div class="widget blogs-widget">
                    <?php if($this->beginCache('application.modules.blog.widgets.BlogsWidget', ['duration' => $this->yupe->coreCacheTime])):?>
                        <?php $this->widget(
                            'application.modules.blog.widgets.BlogsWidget',
                            ['cacheTime' => $this->yupe->coreCacheTime]
                        ); ?>
                        <?php $this->endCache();?>
                    <?php endif;?>
                </div>
            </div>
            </div>
        </div>
    </div>