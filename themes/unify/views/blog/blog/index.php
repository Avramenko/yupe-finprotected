<?php
/**
 * @var $this BlogController
 * @var $form TbActiveForm
 * @var $blogs Blog
 */
$this->title = ['Проекты и дела', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = 'Проекты и дела';
$this->metaKeywords = 'Проекты и дела';
$this->layout = '//layouts/base';
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/pages/blog.css');
Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/pages/blog_magazine.css');
?>

<?php $this->breadcrumbs = [Yii::t('BlogModule.blog', 'Blogs')]; ?>
<!--=== Content Part  ===-->
<div class="container content">
    <div class="row blog-page">
        <div class="col-md-9">
            <?php
            $form = $this->beginWidget(
                'bootstrap.widgets.TbActiveForm',
                [
                    'method' => 'get',
                    'type'   => 'vertical'
                ]
            );
            ?>

            <div class="row">
                <div class="col-sm-12">
                    <div class="input-group">
                        <?php echo $form->textField(
                            $blogs,
                            'name',
                            ['placeholder' => 'Поиск по названию', 'class' => 'form-control']
                        ); ?>
                        <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><?php echo Yii::t('BlogModule.blog', 'search'); ?></button>
                  </span>
                    </div>
                </div>
            </div>

            <?php $this->endWidget(); ?>

            <h1>
                <small>
                    Выполняемые работы и услуги
                    <a
                        href="<?php echo Yii::app()->createUrl('/blog/blogRss/feed/'); ?>">
                        <img
                            src="<?php echo Yii::app()->getTheme()->getAssetsUrl() . "/images/rss.png"; ?>"
                            alt="<?php echo Yii::t('BlogModule.blog', 'Subscribe for updates') ?>"
                            title="<?php echo Yii::t('BlogModule.blog', 'Subscribe for updates') ?>">
                    </a>
                </small>
            </h1>


            <?php
            $this->widget(
                'bootstrap.widgets.TbListView',
                [
                    'dataProvider'       => $blogs->search(),
                    //'template'           => '{sorter}<br/><hr/>{items} {pager}',
                    'template'           => '{items} {pager}',
                    //'sorterCssClass'     => 'sorter pull-left',
                    'itemView'           => '_view',
                    'ajaxUpdate'         => false,
                    'sortableAttributes' => [
                        'name',
                        'postsCount',
                        'membersCount'
                    ],
                ]
            );
            ?>
        </div>
        <div class="col-md-3 magazine-page">


            <?php if($this->beginCache('application.modules.blog.widgets.LastPostsWidget', ['duration' => $this->yupe->coreCacheTime])):?>
                <?php $this->widget(
                    'application.modules.blog.widgets.LastPostsWidget',

                    [
                        'view' => 'lastposts-unify',
                        'cacheTime' => $this->yupe->coreCacheTime,

                    ]
                ); ?>
                <?php $this->endCache();?>
            <?php endif;?>

            <?php if($this->beginCache('application.modules.blog.widgets.BlogsCategoriesMenuWidget', ['duration' => $this->yupe->coreCacheTime])):?>
                <?php $this->widget(
                    'application.modules.blog.widgets.BlogsCategoriesMenuWidget',

                    [
                        'view' => 'index',
                        'cacheTime' => $this->yupe->coreCacheTime,

                    ]
                ); ?>
                <?php $this->endCache();?>
            <?php endif;?>
            <div class="widget blogs-widget">
                <?php if($this->beginCache('application.modules.blog.widgets.BlogsWidget', ['duration' => $this->yupe->coreCacheTime])):?>
                    <?php $this->widget(
                        'application.modules.blog.widgets.BlogsWidget',
                        ['cacheTime' => $this->yupe->coreCacheTime]
                    ); ?>
                    <?php $this->endCache();?>
                <?php endif;?>
            </div>
            <!-- Tabs Widget -->
            <!--<div class="headline headline-md"><h2>Информационный блок</h2></div>
            <div class="tab-v2 margin-bottom-40">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home-1">Команда</a></li>
                    <li><a data-toggle="tab" href="#home-2">Ссылки</a></li>
                </ul>
                <div class="tab-content">
                    <div id="home-1" class="tab-pane active">
                        <p>Vivamus imperdiet condimentum diam, eget placerat felis consectetur id. Donec eget orci metus, ac ac adipiscing nunc.</p> <p>Pellentesque fermentum, ante ac felis consectetur id. Donec eget orci metusvivamus imperdiet.</p>
                    </div>
                    <div id="home-2" class="tab-pane magazine-sb-categories">
                        <div class="row">
                            <ul class="list-unstyled col-xs-6">
                                <li><a href="#">Best Sliders</a></li>
                                <li><a href="#">Parralax Page</a></li>
                                <li><a href="#">Backgrounds</a></li>
                                <li><a href="#">Parralax Slider</a></li>
                                <li><a href="#">Responsive</a></li>
                                <li><a href="#">800+ fa fas</a></li>
                            </ul>
                            <ul class="list-unstyled col-xs-6">
                                <li><a href="#">60+ Pages</a></li>
                                <li><a href="#">Layer Slider</a></li>
                                <li><a href="#">Bootstrap 3</a></li>
                                <li><a href="#">Fixed Header</a></li>
                                <li><a href="#">Best Template</a></li>
                                <li><a href="#">And Many More</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>-->
            <!-- End Tabs Widget -->








        </div>
    </div>
</div>
<!--/container-->
<!--=== End Content Part  ===-->