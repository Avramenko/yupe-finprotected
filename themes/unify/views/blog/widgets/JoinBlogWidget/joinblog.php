<?php if (!$blog->isPrivate()): ?>
    <?php if ($user->isAuthenticated()): ?>
        <?php if (false === $inBlog || UserToBlog::STATUS_DELETED === $inBlog): ?>
            <a class="btn-u btn-u-small join-blog" href="<?= $blog->id; ?>"
               data-url="<?= Yii::app()->createUrl('/blog/blog/join'); ?>">
                Подписаться
            </a>
        <?php elseif ($inBlog == UserToBlog::STATUS_CONFIRMATION): ?>
            <button type="button" class="btn-u btn-u-small btn-info disabled"><?= Yii::t('BlogModule.blog', 'Wait for confirmation'); ?></button>
        <?php
        else: ?>
            <a class="btn-u btn-u-small btn-warning leave-blog" href="<?= $blog->id; ?>"
               data-url="<?= Yii::app()->createUrl('/blog/blog/leave'); ?>">Отписаться</a>
        <?php endif; ?>
    <?php else: ?>
        <a class="btn-u btn-u-small btn-warning"
           href="<?= Yii::app()->createUrl('/user/account/login'); ?>">
            Подписаться
        </a>
    <?php endif; ?>
<?php endif; ?>
