<div class="posts margin-bottom-40">
    <div class="headline headline-md"><h2>Последние события</h2></div>
    <?php foreach ($models as $model): ?>
        <dl class="dl-horizontal">
            <dt><a href="#">
                    <?php echo CHtml::link(CHtml::image(
                        $model->getImageUrl(64,64),
                        CHtml::encode($model->title),
                        [
                            'width' => 64,
                            'height' => 64,
                            'class' => 'img-responsive'
                        ]
                    ),
                        $model->getUrl()
                    ); ?>

                </a>
            </dt>
            <dd>
                <p><?php echo CHtml::link(
                        CHtml::encode($model->title),
                        $model->getUrl()
                    ); ?></p>
            </dd>
        </dl>

    <?php endforeach; ?>

</div>
