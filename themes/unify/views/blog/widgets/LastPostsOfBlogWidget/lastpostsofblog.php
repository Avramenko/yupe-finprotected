<?php Yii::import('application.modules.blog.BlogModule'); ?>
<?php foreach ($posts as $post): ?>
<!-- News v3 -->
<div class="row margin-bottom-20 margin-top-20">
    <div class="col-sm-5 sm-margin-bottom-20 ">
        <?php echo $post->getImageUrl() ? CHtml::image($post->getImageUrl(), CHtml::encode($post->title), ['class' => 'img-responsive']) : ''; ?>

    </div>
    <div class="col-sm-7 news-v3">
        <div class="news-v3-in-sm no-padding">
            <ul class="list-inline posted-info">
                <li><?php $this->widget(
                        'application.modules.user.widgets.UserPopupInfoWidget',
                        [
                            'model' => $post->createUser
                        ]
                    ); ?></li>

                <li><?php echo Yii::app()->getDateFormatter()->formatDateTime(
                        $post->publish_time,
                        "long",
                        "short"
                    ); ?></li>
            </ul>
            <ul class="list-unstyled list-inline blog-tags">
                <li>
                    <i class="fa fa-tags"></i>



            <?php foreach ((array)$post->getTags() as $tag): ?>

                <?php echo CHtml::link(
                    CHtml::encode($tag),
                    ['/posts/', 'tag' => CHtml::encode($tag)]
                ); ?>

            <?php endforeach; ?>

                </li>
                <li><i class="glyphicon glyphicon-comment"></i>

                    <?php echo CHtml::link(
                        $post->getCommentCount(),
                        $post->getUrl(['#' => 'comments'])
                    ); ?>
                </li>

            </ul>
            <h2><?php echo CHtml::link(
                    CHtml::encode($post->title),
                    $post->getUrl()
                ); ?></h2>
            <?php echo strip_tags($post->getQuote()); ?>
            <ul class="post-shares">
                <li>
                    <a href="#">
                        <i class="rounded-x icon-speech"></i>
                        <span>5</span>
                    </a>
                </li>
                <li><a href="#"><i class="rounded-x icon-share"></i></a></li>
                <li><a href="#"><i class="rounded-x icon-heart"></i></a></li>
            </ul>
        </div>
    </div>
</div><!--/end row-->
<!-- End News v3 -->

<div class="clearfix margin-bottom-20"><hr></div>

<?php endforeach; ?>
