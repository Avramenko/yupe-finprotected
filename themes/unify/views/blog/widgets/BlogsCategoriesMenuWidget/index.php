<?php
//\CVarDumper::dump($data);
?>
<!-- Blog Tags -->
<div class="headline headline-md"><h2>Проекты и дела</h2></div>

<ul class="list-unstyled blog-tags margin-bottom-30">
    <?php foreach($data as $key => $value){?>
        <li><a href="/blog/category/<?=$value['slug']?>"><i class="fa fa-tags"></i> <?=CHtml::encode($value['name']);?></a></li>
    <?php }?>

</ul>
<!-- End Blog Tags -->