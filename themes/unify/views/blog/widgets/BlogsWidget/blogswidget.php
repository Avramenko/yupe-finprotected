<div class="posts">


<div class="headline headline-md"><h2>Работы и услуги</h2></div>
    <?php foreach ($models as $model): ?>
        <dl class="dl-horizontal">
            <dt>
                    <?php echo CHtml::link(
                        CHtml::image(
                        $model->getImageUrl(64,64),
                        CHtml::encode($model->name),
                            [
                                'width' => 64,
                                'height' => 64,
                                'class' => 'img-responsive'
                            ]
                        ),
                        ['/blog/blog/show/', 'slug' => CHtml::encode($model->slug)]
                    ); ?>

            </dt>
            <dd>
                <p><?php echo CHtml::link(
                        CHtml::encode($model->name),
                        ['/blog/blog/show/', 'slug' => CHtml::encode($model->slug)]
                    ); ?>
                    &rarr;
                    <i class="glyphicon glyphicon-user"></i>
                    <?php echo CHtml::link(
                        $model->membersCount,
                        ['/blog/blog/members', 'slug' => CHtml::encode($model->slug)]
                    ); ?>
                    &rarr;
                    <i class="glyphicon glyphicon-file"></i>
                    <?php echo CHtml::link(
                        $model->postsCount,
                        ['/blog/post/blog/', 'slug' => CHtml::encode($model->slug)]
                    ); ?></p>
            </dd>
        </dl>
    <?php endforeach; ?>

</div>