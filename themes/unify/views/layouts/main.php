<?php $this->beginContent('//layouts/base'); ?>
<!--=== Content Part  ===-->
    <div class="container content">
        <div class="row">
            <div class="col-md-12">
                <?php echo $content; ?>
            </div>
        </div>
    </div>
<!--/container-->
<!--=== End Content Part  ===-->
<?php $this->endContent(); ?>
