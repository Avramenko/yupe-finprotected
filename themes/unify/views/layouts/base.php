<!DOCTYPE html>
<!--[if IE 8]> <html lang="<?php echo Yii::app()->language; ?>" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="<?php echo Yii::app()->language; ?>" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="<?php echo Yii::app()->language; ?>"> <!--<![endif]-->
<head>
    <?php Yii::app()->controller->widget(
        'vendor.chemezov.yii-seo.widgets.SeoHead',
        [
            'httpEquivs'         => array(
                'Content-Type'     => 'text/html; charset=utf-8',
                'X-UA-Compatible'  => 'IE=edge,chrome=1',
                'Content-Language' => Yii::app()->language
            ),
            'defaultTitle'       => Yii::app()->getModule('yupe')->siteName,
            'defaultDescription' => Yii::app()->getModule('yupe')->siteDescription,
            'defaultKeywords'    => Yii::app()->getModule('yupe')->siteKeyWords,
        ]
    ); ?>

    <!-- Web Fonts -->
    <link rel="shortcut" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600&subset=cyrillic,latin">

    <?php
    $mainAssets = Yii::app()->getTheme()->getAssetsUrl();

    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/main.css');
    // yupe css styles
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/flags.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/yupe.css');
    // end yupe css styles

    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/bootstrap/css/bootstrap.min.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/style.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/headers/header-v4.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/footers/footer-v1.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/animate.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/line-icons/line-icons.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/font-awesome/css/font-awesome.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/theme-colors/purple.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/owl-carousel/owl-carousel/owl.carousel.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/parallax-slider/css/parallax-slider.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/theme-skins/dark.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/custom.css');

    // yupe JS
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/blog.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/bootstrap-notify.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/jquery.li-translit.js');


    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/app.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/back-to-top.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/smoothScroll.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/parallax-slider/js/modernizr.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/parallax-slider/js/jquery.cslider.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/owl-carousel/owl-carousel/owl.carousel.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/owl-carousel.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/parallax-slider.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/custom.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/app.js');
    ?>
    <script type="text/javascript">
        var yupeTokenName = '<?php echo Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?php echo Yii::app()->getRequest()->csrfToken;?>';
    </script>
    <style>
        .header-v4 .dropdown > a:after{
            content: ""
        }
    </style>
</head>
<body>
<!-- Google-->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-29492828-2', 'auto');
    ga('send', 'pageview');

</script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-66174023-1', 'auto');
    ga('send', 'pageview');

</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter31888821 = new Ya.Metrika({
                    id:31888821,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/31888821" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


<!-- Wrapper -->
<?php echo Chtml::tag('div',['class'=>'wrapper'])?>

<!--=== Header ===-->
<!--=== Header ===-->
<div class="header-v4">

        <!-- Logo -->
        <!--<a class="logo" href="/">
            <img src="<?/*=$mainAssets;*/?>/images/logo.png" alt="Logo">
        </a>-->
        <!-- End Logo -->

        <!-- Topbar -->
        <div class="topbar-v1">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <ul class="list-inline top-v1-contacts">
                            <li>
                                <i class="fa fa-envelope"></i> Email: <a href="mailto:ceo@finprotected.com">ceo@finprotected.com</a>
                            </li>
                            <li>
                                <i class="fa fa-phone"></i> Телефон: <a href="tel:+7 (905) 232-11-11">+7(905)232-11-11</a>
                            </li>
                        </ul>
                    </div>

                    <!--<div class="col-md-6">
                        <ul class="list-inline top-v1-data">
                            <li><a href="#"><i class="fa fa-home"></i></a></li>
                            <li><a href="#"><i class="fa fa-globe"></i></a></li>
                        </ul>
                    </div>-->
                </div>
            </div>
        </div>
        <!-- End Topbar -->



    <!-- Navbar -->
    <div class="navbar navbar-default mega-menu" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <div class="row">
                    <div class="col-md-2">
                        <a class="navbar-brand" href="/">
                            <img id="logo-header" src="<?=$mainAssets;?>/images/logo.png" alt="Logo">
                            <!--<img id="logo-header" src="assets/img/logo1-default.png" alt="Logo">-->
                        </a>
                    </div>
                    <div class="col-md-10">

                        <a href="/blog/category/vozvrat-sredstv-investorov-mmcis-i-mill-trade"><img class="header-banner img-responsive" src="<?=$mainAssets;?>/img/banners/adds.jpg" width="1000" alt=""></a>

                    </div>
                </div>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="full-width-menu">Меню</span>
                        <span class="icon-toggle">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </span>
                </button>
            </div>
        </div>

        <div class="clearfix"></div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-responsive-collapse">
            <div class="container">
                <?php if (Yii::app()->hasModule('menu')): ?>
                    <?php $this->widget(
                        'application.modules.menu.widgets.MenuWidget', [
                            'name' => 'top-menu',
                            'layout' => 'top-menu'
                        ]
                    ); ?>
                <?php endif; ?>
            </div>
        </div>

    </div>

</div>
<!--=== End Header ===-->


    <?php /*echo Chtml::tag('div',['class'=>'header-v6 header-classic-white header-sticky'])*/?><!--
        <?php /*echo Chtml::tag('div',['class'=>'navbar mega-menu', 'role'=>'navigation'])*/?>
            --><?php /*echo Chtml::tag('div',['class'=>'container'])*/?>


                <?php /*if (Yii::app()->hasModule('menu')): */?><!--
                    <?php /*$this->widget(
                        'application.modules.menu.widgets.MenuWidget', [
                            'name' => 'top-menu',
                        ]
                    ); */?>
                --><?php /*endif; */?>

            <?php /*echo Chtml::closeTag('div')*/?><!--
        <?php /*echo Chtml::closeTag('div')*/?>
    --><?php /*echo Chtml::closeTag('div')*/?>
<!--=== End Header ===-->

<?php if(Yii::app()->getRequest()->requestUri == '/'){?>
    <!--=== Slider ===-->
    <?php $this->widget(
        "application.modules.contentblock.widgets.ContentBlockWidget",
        array("code" => "banner-na-glavnoy"));
    ?>
    <!--=== End Slider ===-->
<?php }?>



<!-- breadcrumbs -->
<?php /*$this->widget(
    'bootstrap.widgets.TbBreadcrumbs',
    [
        'links' => $this->breadcrumbs,
    ]
);*/?>
<!-- end breadcrumbs -->

<!-- content -->

<?php echo $content; ?>

<!-- end content -->

<?php $this->widget(
    "application.modules.contentblock.widgets.ContentBlockWidget",
    array("code" => "podval"));
?>


<?php echo Chtml::closeTag('div')?>
<!-- /.wrapper -->

<script>
    jQuery(document).ready(function() {
        App.init();
        OwlCarousel.initOwlCarousel();
        ParallaxSlider.initParallaxSlider();
    });
</script>
<!--[if lt IE 9]>
<script src="<?php echo $mainAssets?>/plugins/respond.js"></script>
<script src="<?php echo $mainAssets?>/plugins/html5shiv.js"></script>
<script src="<?php echo $mainAssets?>/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
</body>
</html>