<?php
/* @var $data News */
?>

<div class="col-md-6">
    <div class="magazine-news-img">
        <?php echo CHtml::link(CHtml::image($data->getImageUrl(555,182), $data->title, ['class'=>'img-responsive']), $data->getUrl()); ?>
        <?php if($data->category_id !== null){?>
            <span class="magazine-badge label-purple"><?=$data->category->name;?></span>
        <?php }?>
    </div>
    <h3><?php echo CHtml::link(CHtml::encode($data->title), $data->getUrl()); ?></h3>

    <?php echo $data->short_text?>
    <div class="by-author">
        <span>Дата: <?php echo Yii::app()->dateFormatter->formatDateTime($data->date, 'long', null)?></span>
    </div>
    <?php /*echo CHtml::link(Yii::t('NewsModule.news', 'read...'), $data->getUrl(), ['class' => 'btn btn-default']); */?>
    <div class="margin-bottom-35"><hr class="hr-md"></div>
</div>

