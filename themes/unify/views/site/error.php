<?php
$this->layout = '404';
$this->title = [Yii::t('default', 'Error') . ' ' . $error['code'], Yii::app()->getModule('yupe')->siteName]; ?>
<?php
switch ($error['code']) {
    case '404':
        $msg = Yii::t(
            'default',
            'Page you try to request, was not found. You can go out from this page and {link}.',
            [
                '{link}' => CHtml::link(
                    Yii::t('default', 'go to home page'),
                    $this->createAbsoluteUrl('/'),
                    [
                        'title' => Yii::t('default', 'go to home page'),
                        'alt'   => Yii::t('default', 'go to home page'),
                    ]
                ),

            ]
        );
        break;
    default:
        $msg = $error['message'];
        break;
}
?>


<!--=== Content Part ===-->
<div class="container">
    <!--Error Block-->
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="error-v2">
                <span class="error-v2-title"><?=$error['code']?></span>
                <span><?= Yii::t('default', 'Error'); ?>!</span>
                <p><?= $msg; ?></p>
            </div>
        </div>
    </div>
    <!--End Error Block-->
</div><!--/container-->
<!--=== End Content Part ===-->

